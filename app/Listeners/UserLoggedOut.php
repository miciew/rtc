<?php

namespace App\Listeners;


class UserLoggedOut
{
    public function handle($event)
    {
        $event->user->fill(['online' => false])
            ->save();

        broadcast(new \App\Events\UserLoggedout($event->user));
    }
}