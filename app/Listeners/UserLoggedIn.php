<?php

namespace App\Listeners;


class UserLoggedIn
{
    public function handle($event)
    {
        $event->user->fill(['online' => true])
            ->save();

        broadcast(new \App\Events\UserLoggedin($event->user));
    }
}