<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Events\MessageWasDisliked;
use App\Events\MessageWasLiked;
use App\Like;
use App\Message;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /** @var Guard  */
    protected $guard;

    /**
     * MessageController constructor.
     * @param $guard
     */
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');

        $this->guard = $guard;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Chat $chat)
    {
        $this->validate($request, [
            'message' => 'required|min:1'
        ]);

        /** @var Message $message */
        $message = $this->guard->user()->messages()->create([
            'chat_id' => $chat->id,
            'body' => $request->get('message')
        ]);

        return $message->load(['owner']);
    }

    public function toogleLike(Request $request, Message $message)
    {
        $toogleLike = $this->guard->user()->toogleLikeMessage($message);

        if( $toogleLike instanceof Like )
            return $toogleLike;

        return response()->json($toogleLike);
    }
}
