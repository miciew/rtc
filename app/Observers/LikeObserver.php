<?php

namespace App\Observers;


use App\Events\MessageWasDisliked;
use App\Events\MessageWasLiked;
use App\Like;

class LikeObserver
{
    public function created(Like $like)
    {
        broadcast(new MessageWasLiked($like));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleting(Like $like)
    {
        broadcast(new MessageWasDisliked($like));
    }
}