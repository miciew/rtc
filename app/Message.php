<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use App\Events\ChatMessageWasRecieved;

class Message extends Model
{
    protected $fillable = ['body', 'chat_id'];

    protected $with = [ 'chat', 'owner', 'likes' ];

    protected $appends = [ 'likes_count' ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public static function boot ()
    {
        parent::boot();

        self::created(function ($message) {

            broadcast(new ChatMessageWasRecieved($message));

        });
    }

    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }
}
