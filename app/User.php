<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname', 'password', 'online'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function getNameAttribute($value = '')
    {
        return $this->nickname;
    }

    public function scopeIsOnline($builder)
    {
        return $builder->where('online', '=', 1);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function likeMessage(Message $message)
    {
        return $message->likes()->create(['user_id' => $this->id]);
    }

    public function dislikeMessage(Message $message)
    {
        $like = $message->likes()
            ->where('user_id', '=',$this->id)
            ->first()
            ->delete();

        return $like;
    }

    public function toogleLikeMessage(Message $message)
    {
        $wasLiked = $message->likes()
            ->where('user_id', '=', $this->id)
            ->exists();

        if( $wasLiked )
            return $this->dislikeMessage($message);

        return $this->likeMessage($message);
    }
}
