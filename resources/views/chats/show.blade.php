@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $chat->title }}</h1>

        @if( $errors->count() )
            @component('alert')
                {{ $errors }}
            @endcomponent
        @endif

        <div class="row">
            <div class="col-md-2">
                <online-users></online-users>
            </div>
            <div class="col-md-10">
                <messages chat="{{ json_encode($chat) }}"></messages>
            </div>
        </div>

    </div>
@endsection