@extends('layouts.app')

@section('content')
    <div class="container">
        <ul class="list-group">

            @foreach($chats as $chat)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <a href="{{ route('chats.show', $chat->id) }}">{{ $chat->title }}</a>
                    <span class="badge badge-primary badge-pill">{{ $chat->messages()->count() }}</span>
                </li>
            @endforeach
        </ul>
    </div>
@endsection