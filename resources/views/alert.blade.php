<div class="alert alert-danger">
    <div class="alert-title">{{ $title or '' }}</div>

    {{ $slot }}
</div>