<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('/chats', 'ChatController');
Route::get('/chats/{chat}/messages', 'ChatController@messages')->name('chats.messages');

Route::post('/chats/{chat}/messages', 'MessageController@store')->name('messages.store');

Route::get('/users/online', 'ChatController@onlineUsers')->name('users.online');

Route::post('/messages/{message}/like', 'MessageController@toogleLike')->name('messages.like');
